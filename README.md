# auto-documentation

Docfx project to automatically generate documentation for all library projects

The generated documentation can be found [here](https://martensi1.gitlab.io/pilot-app-libs/auto-documentation)


## License

This repository is licensed with the [MIT](LICENSE) license

## Author

martensi (Simon Mårtensson)


