<#
.SYNOPSIS
    .
.DESCRIPTION
    .
.PARAMETER DocfxPath
    Path to the docfx project. It's either a path to a csproj or directory. If not 
    specified, it will default to "docs"
.PARAMETER NonInteractive
    If specified, the script will not pause at end of execution and wait for user input
#>
param(
  [string]$DocfxPath = "Docs.csproj",
  [switch]$NonInteractive
)

# Setup
. $PSScriptRoot/ci/scripts/common.ps1
PrintScriptTitle -Title "Build Documentation"


# Main
$projects = $(Get-ChildItem "projects/" -Directory)
foreach($project in $projects) {
  dotnet build $(Join-Path -Path $project -ChildPath "src/")
  ThrowIfOperationFailed
}

dotnet build $DocfxPath
ThrowIfOperationFailed


# Pause if interactive
if (!$NonInteractive) {
  PressAnyKey
}